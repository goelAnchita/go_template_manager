package entity

type TemplateEntity struct {
	Data    string
	Name    string
	Version int
}
