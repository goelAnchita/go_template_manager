package util

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"strconv"
	"strings"
)

func init() {
	flag.Parse()
}

func StartsWith(searchString string, prefix string) bool {
	return strings.HasPrefix(searchString, prefix)
}

func EndsWith(searchString string, suffix string) bool {
	retVal := strings.HasSuffix(searchString, suffix)
	//glog.Infof("EndsWith retruning %s", retVal)
	return retVal
}

func AssertEven(values []string) error {

	if len(values)%2 != 0 {
		return errors.New("Invalid dict call. Number of arguments should be even")
	}

	return nil
}

func CreateJsonString(values ...string) (string, error) {

	if err := AssertEven(values); err != nil {
		return "", err
	}

	dict := make(map[string]string, len(values)/2)

	for i := 0; i < len(values); i += 2 {
		dict[values[i]] = values[i+1]
	}

	data, err := json.Marshal(dict)

	if err != nil {
		return "", err
	}

	return string(data), nil
}

func CreateCSV(array []map[string]interface{}, key string) (response string, err error) {

	length := len(array)

	if length == 0 {
		return "", errors.New("Empty array passed")
	}

	string_array := make([]string, length, length)
	string_value := ""
	for i := 0; i < length; i++ {
		if value, ok := array[i][key]; ok {
			switch v := value.(type) {
			default:
				errorMessage := fmt.Sprintf("Unexpected value type in CreateCSV %s", v)
				return "", errors.New(errorMessage)
			case float64:
				string_value = fmt.Sprintf("\"%s\"", strconv.FormatFloat(value.(float64), 'E', 2, 64))
			case string:
				string_value = fmt.Sprintf("\"%s\"", value.(string))
			case int64:
				string_value = fmt.Sprintf("\"%s\"", strconv.FormatInt(value.(int64), 10))
			}
			string_array[i] = string_value

		} else {
			return "", errors.New("Value corresponding to the key not found")
		}
	}

	response = strings.Join(string_array, ",")
	//glog.Infof("%s", response)
	return response, nil

}
