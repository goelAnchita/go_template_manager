package service

type StylesConf struct {
	Maximus MaximusConf
	Ultron  UltronConf
}

type MaximusConf struct {
	URL string
}

type CartConf struct {
	AbsolutURL string
}

type IdeaConf struct {
	IdeaURL string
}

type UltronConf struct {
	URL string
}

type MyntraServiceConf struct {
	Cart                   CartConf
	Idea                   IdeaConf
	Styles                 StylesConf
	HTTPMaxIdleConnections int
}

var (
	ServiceConf MyntraServiceConf
)
