package userDetails

import "github.com/golang/glog"

func GetUserByEmail(email string) (userDetails map[string]interface{}, err error) {

	ideaClient := NewIdeaClient()
	userData, error := ideaClient.GetUserByEmail(email)

	if error != nil {
		glog.Errorf("Error in getting user data %s", error)
		return nil, error
	}

	return userData, error
}

func GetUserByUidx(uidx string) (userDetails map[string]interface{}, err error) {

	ideaClient := NewIdeaClient()
	userData, error := ideaClient.GetUserByUidx(uidx)

	if error != nil {
		glog.Errorf("Error in getting user data %s", error)
		return nil, error
	}

	return userData, error
}
