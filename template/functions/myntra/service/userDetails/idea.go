package userDetails

import (
	"actionexecutor/libs/template/functions/myntra/service"
	"actionexecutor/libs/util"
	"encoding/json"
	"github.com/golang/glog"
	"io/ioutil"
	"net"
	"net/http"
	"time"
)

type Idea_Client struct {
	http_client *http.Client
	http_header http.Header
}

const (
	//MaxIdleConnections  int           = 1
	KeepAliveConnection bool          = true
	RequestTimeout      time.Duration = time.Duration(60) * time.Second
)

var (
	IDEA_GET_BY_UIDX  string = service.ServiceConf.Idea.IdeaURL + "idea/opt/profile/uidx/myntra/"
	IDEA_GET_BY_EMAIL        = service.ServiceConf.Idea.IdeaURL + "idea/opt/profile/email/myntra/"
	transport         *http.Transport
	idea_client       *Idea_Client
)

func init() {
	transport = &http.Transport{
		DisableKeepAlives:   !KeepAliveConnection,
		MaxIdleConnsPerHost: service.ServiceConf.HTTPMaxIdleConnections,
	}

	header := make(http.Header)
	header.Add("Accept", "application/json")
	header.Add("Connection", "keep-alive")

	client := &http.Client{Transport: transport}
	idea_client = &Idea_Client{http_header: header, http_client: client}

}

func defaultDial(network, addr string) (net.Conn, error) {
	conn, err := net.DialTimeout(network, addr, RequestTimeout)
	if err != nil {
		conn.SetDeadline(time.Now().Add(RequestTimeout))
	}
	return conn, err
}

func NewIdeaClient() *Idea_Client {
	return idea_client
}

func (client *Idea_Client) GetUserByUidx(uidx string) (map[string]interface{}, error) {
	url := IDEA_GET_BY_UIDX + uidx
	return client.GetUser(url)
}

func (client *Idea_Client) GetUserByEmail(email string) (map[string]interface{}, error) {
	url := IDEA_GET_BY_EMAIL + email
	return client.GetUser(url)
}

func (client *Idea_Client) GetUser(url string) (map[string]interface{}, error) {

	var response *http.Response = nil

	request, err := http.NewRequest("GET", url, nil)

	if err != nil {
		glog.Errorf("unable to make request: %s", err)
		return nil, err
	}

	//request.Close = true
	request.Header = client.http_header
	response, err = util.HttpRetry(request, client.http_client, "")

	if err != nil {
		if response == nil {
			glog.Errorf("Something went wrong with error while doing http_client.Do %s", err.Error())
		} else if response != nil {
			glog.Info("Something went wrong with error while doing http_client.Do %s", err.Error(), response.StatusCode, response)
			body, _ := ioutil.ReadAll(response.Body)
			glog.Error(string(body))

		}
		return nil, err
	}

	userData, err := responseReply(response)
	if err != nil {
		return nil, err
	}

	if userData != nil {
		userInfo := make(map[string]interface{})
		userInfo["gender"] = userData["gender"]
		userInfo["status"] = userData["status"]
		userInfo["dob"] = userData["dob"]
		userInfo["verified"] = userData["verified"]
		userInfo["phoneDetails"] = userData["phoneDetails"]
		userInfo["id"] = userData["id"]
		userInfo["firstName"] = userData["firstName"]
		userInfo["lastName"] = userData["lastName"]
		userInfo["location"] = userData["location"]
		userInfo["registrationOn"] = userData["registrationOn"]
		userInfo["userType"] = userData["userType"]
		userInfo["uidx"] = userData["uidx"]

		if emailDetails, ok := userData["emailDetails"]; ok {
			userInfo["email"] = (emailDetails.([]interface{}))[0].(map[string]interface{})["email"]
		}
		//glog.Infof("User data is %s", userInfo)
		return userInfo, nil
	}
	return nil, nil

}

func responseReply(resp *http.Response) (response map[string]interface{}, err error) {

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		glog.Errorf("Something went wrong while ioutil.ReadAll with error %s", err.Error())
		return
	}

	defer resp.Body.Close()

	var responseData map[string]interface{}

	if err := json.Unmarshal(body, &responseData); err != nil {
		glog.Errorf("Something went wrong while unmarshalling the response body with %s", err.Error())
		return nil, err
	}

	//glog.Info(responseData)
	if data, ok := responseData["entry"]; ok {
		return data.(map[string]interface{}), nil

	}

	glog.Errorf("No user data found!!, Returning nil")
	return nil, err
}
