package cart_service

import (
	"actionexecutor/libs/template/functions/myntra/service/style"
	"actionexecutor/libs/util"
	"actionexecutor/libs/util/statsd"
	"encoding/json"
	"github.com/golang/glog"
	"io/ioutil"
	"net"
	"net/http"
	"time"
	"actionexecutor/libs/template/functions/myntra/service"
)

type Absolut_Client struct {
	http_client *http.Client
	http_header http.Header
}

const (
	//MaxIdleConnections  int           = 1
	KeepAliveConnection bool          = true
	RequestTimeout      time.Duration = time.Duration(60) * time.Second
)

var (
	EndURI         string = service.ServiceConf.Cart.AbsolutURL + "/myntra-absolut-service/securedabsolut/cart/id/"
	transport      *http.Transport
	absolut_client *Absolut_Client
)

func init() {
	//glog.Infof("Initialising with %v", config.GlobalConfig.HTTPMaxIdleConnections)
	transport = &http.Transport{
		DisableKeepAlives:   !KeepAliveConnection,
		MaxIdleConnsPerHost: service.ServiceConf.HTTPMaxIdleConnections,
	}

	header := make(http.Header)
	header.Add("Accept", "application/json")
	header.Add("Connection", "keep-alive")

	client := &http.Client{Transport: transport}
	absolut_client = &Absolut_Client{http_header: header, http_client: client}

}

func defaultDial(network, addr string) (net.Conn, error) {
	conn, err := net.DialTimeout(network, addr, RequestTimeout)
	if err != nil {
		conn.SetDeadline(time.Now().Add(RequestTimeout))
	}
	return conn, err
}

func NewAbsolutClient() *Absolut_Client {
	return absolut_client

}

func (client *Absolut_Client) GetCartData(cart_id string) (map[string]interface{}, error) {
	var response *http.Response = nil

	defer func() {
		if response != nil {
			response.Body.Close()
		}
	}()

	cartURI := EndURI + cart_id
	//glog.Errorf("Cart url is : ", cartURI)
	request, err := http.NewRequest("GET", cartURI, nil)

	if err != nil {
		glog.Errorf("unable to make request: %s", err)
		return nil, err
	}

	//request.Close = true
	request.Header = client.http_header
	response, err = util.HttpRetry(request, client.http_client, "")

	if err != nil {
		if response == nil {
			glog.Info("Something went wrong with error while doing http_client.Do %s", err.Error())
		} else if response != nil {
			glog.Info("Something went wrong with error while doing http_client.Do %s", err.Error(), response.StatusCode, response)
			body, _ := ioutil.ReadAll(response.Body)
			glog.Error(string(body))
			//glog.Errorf("received from cart : ", (response))
		}
		return nil, err
	}

	//glog.Errorf("received from cart : ", (response))
	return responseReply(response)
}

func GetStylesInCart(cart_id string) (response []map[string]interface{}, err error) {

	cart_data, err := ValidateCart(cart_id)
	if cart_data != nil {
		cart_items := cart_data["cartItemEntries"].([]interface{})
		cart_items_length := len(cart_items)
		stylesInCart := make([]map[string]interface{}, cart_items_length)
		for i := 0; i < cart_items_length; i++ {
			style_item := cart_items[i].(map[string]interface{})
			styleData, err := style.GetStyleData(style_item["styleId"])
			if err != nil {
				return nil, err
			}
			styleData["quantity"] = style_item["quantity"]
			stylesInCart[i] = styleData
		}

		return stylesInCart, err
	}
	return nil, err
}

func GetCartDetails(cart_id string) (response map[string]interface{}, err error) {

	cart_data, err := ValidateCart(cart_id)
	if cart_data != nil {
		cart_items := cart_data["cartItemEntries"].([]interface{})
		cart_items_length := len(cart_items)
		stylesInCart := make([]map[string]interface{}, cart_items_length)
		for i := 0; i < cart_items_length; i++ {
			style_item := cart_items[i].(map[string]interface{})
			styleData, err := style.GetStyleData(style_item["styleId"])
			if err != nil {
				return nil, err
			}
			styleData["quantity"] = style_item["quantity"]
			stylesInCart[i] = styleData
		}
		cart_data["items"] = stylesInCart
		if cart_data["totalCouponDiscount"].(float64) > 0.0 {
			cart_data["couponApplied"] = true
		} else {
			cart_data["couponApplied"] = false
		}

		return cart_data, nil
	}
	return nil, err
}

func ValidateCart(cart_id string) (response map[string]interface{}, err error) {

	cartClient := NewAbsolutClient()
	cart_data, error := cartClient.GetCartData(cart_id)

	if error != nil {
		glog.Errorf("Error in getting cart data %s", error)
		statsd.Increment("workflow.validateCart.error")
		return nil, error
	}
	if cart_items, ok := cart_data["cartItemEntries"]; ok {
		if len(cart_items.([]interface{})) > 0 {
			//glog.Infof("Received cart_data %s", cart_data)
			return cart_data, nil
		}

	}

	return nil, error

}

func responseReply(resp *http.Response) (response map[string]interface{}, err error) {

	//glog.Infof("response header %v", resp.Header)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		glog.Errorf("Something went wrong while ioutil.ReadAll with error %s", err.Error())
		return
	}

	var responseData map[string]interface{}

	if err := json.Unmarshal(body, &responseData); err != nil {
		glog.Errorf("Something went wrong while unmarshalling the response body with %s", err.Error())
		return nil, err
	}
	if data, ok := responseData["data"]; ok {
		if len(data.([]interface{})) > 0 {
			data_objects := data.([]interface{})
			cart_data := data_objects[0].(map[string]interface{})
			response = cart_data
			return
		}

	}

	glog.Errorf("No cart data found!!!, Returning nil")
	return nil, err
}
