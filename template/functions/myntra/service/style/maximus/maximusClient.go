package maximus

import (
	"actionexecutor/actionExecutor/config"
	"errors"
	"git.apache.org/thrift.git/lib/go/thrift"
	"github.com/golang/glog"
	"net/http"
	"os"
)

var maximusClient *MaximusServiceClient

var (
	maximus_url         string = config.GlobalConfig.MaximusURL
	KeepAliveConnection bool   = true
	transport           *http.Transport
	thttpClient         thrift.THttpClientOptions
)

func init() {

	transport = &http.Transport{DisableKeepAlives: !KeepAliveConnection,
		MaxIdleConnsPerHost: config.GlobalConfig.HTTPMaxIdleConnections}

	var err error = nil
	maximusClient, err = getClient()

	if err != nil {
		glog.Error(os.Stderr, "Error while creating maximus client", err)
	}
}

func MaximusClient() (*MaximusServiceClient, error) {
	return getClient()
}

func getClient() (*MaximusServiceClient, error) {
	thttpClient.Client = &http.Client{Transport: transport}
	trans, err := thrift.NewTHttpPostClientWithOptions(maximus_url, thttpClient)
	if err != nil {
		glog.Error(os.Stderr, "Error creating transport", err)
		return nil, err
	}

	c := NewMaximusServiceClientFactory(trans, thrift.NewTBinaryProtocolFactoryDefault())
	if err := trans.Open(); err != nil {
		glog.Error("ERROR OPENINING SOCKET MAXIMUS CLIENT")
		return nil, err
	}
	return c, nil
}

func (mc *MaximusServiceClient) GetStyleData(styleId int64, arg1 Fields) (stylemap map[string]interface{}, err error) {

	pdpResponse, err := mc.GetPdpData(styleId, arg1)

	defer func() {
		if r := recover(); r != nil {
			//glog.Errorf("Panic attack with GetStyleData %+v  with pdpResponse %+v for style id %+v ", r, pdpResponse, styleId)
			err = r.(error)
		}
		mc.Transport.Close()
	}()

	if err != nil {
		glog.Errorf("Error %s in getting PDP data from Maximus for style id %v", err, styleId)
		return nil, err
	}

	styleMap := make(map[string]interface{})
	if pdpResponse != nil && pdpResponse.Data != nil && len(pdpResponse.Data) > 0 && pdpResponse.Data[0] != nil {
		styleCmsData := pdpResponse.Data[0].CmsData
		if styleCmsData != nil {
			styleMap["styleID"] = *(styleCmsData.ID)
			styleMap["productDisplayName"] = *(styleCmsData.ProductDisplayName)
			styleMap["gender"] = *(styleCmsData.Gender)
			styleMap["landingPageUrl"] = *(styleCmsData.LandingPageUrl)
			styleMap["brandName"] = *(styleCmsData.BrandName)
			styleMap["styleName"] = *(styleCmsData.Name)
			styleMap["mrp"] = *(styleCmsData.Price)
			styleMap["isExclusive"] = *(styleCmsData.VisualTag)
			styleMap["attributes"] = styleCmsData.ArticleAttributes
			if styleCmsData.ArticleType != nil {
				styleMap["articleTypeName"] = *(styleCmsData.ArticleType.TypeName)
				styleMap["category"] = *(styleCmsData.ArticleType.TypeName)
			} else {
				return nil, errors.New("No Article type data")
			}
			if styleCmsData.StyleImages != nil {
				if imageSrc, ok := styleCmsData.StyleImages["default"]; ok {
					styleMap["imgSrc"] = imageSrc.Resolutions["180X240"]
					styleMap["imageUrls"] = imageSrc.Resolutions
				} else if imageSrc, ok := styleCmsData.StyleImages["front"]; ok {
					styleMap["imgSrc"] = imageSrc.Resolutions["180X240"]
					styleMap["imageUrls"] = imageSrc.Resolutions
				} else {
					styleMap["imgSrc"] = imageSrc.Resolutions["180X240"]
					styleMap["imageUrls"] = imageSrc.Resolutions
				}
			} else {
				return nil, errors.New("No style image source data")
			}
		} else {
			return nil, errors.New("No style CMS data")
		}

		styleOptionsData := pdpResponse.Data[0].StyleOptionsData
		if styleOptionsData != nil && styleOptionsData.StyleOptions != nil && (len(styleOptionsData.StyleOptions)) > 0 {
			var inventoryCount int32
			skuSizeMap := make(map[int64]string)
			for _, styleOp := range styleOptionsData.StyleOptions {
				inventoryCount += *(styleOp.InventoryCount)
				skuSizeMap[*(styleOp.SkuId)] = *(styleOp.Value)
			}
			styleMap["oneSize"] = false
			if *(styleOptionsData.StyleOptions[0].Value) == "Onesize" {
				styleMap["oneSize"] = true
			}

			styleMap["inventoryCount"] = inventoryCount
			styleMap["skuSizeMap"] = skuSizeMap
		} else {
			return nil, errors.New("No style Options data")
		}

		styleDiscountData := pdpResponse.Data[0].DiscountData
		styleMap["onDiscount"] = false
		styleMap["price"] = styleMap["mrp"]
		styleMap["discountedPrice"] = styleMap["mrp"]

		if styleDiscountData != nil && styleDiscountData.DiscountData != nil && styleDiscountData.DiscountData.DiscountPercent != nil {
			styleMap["discountedPrice"] = *(styleDiscountData.DiscountedPrice)
			styleMap["discount"] = *(styleDiscountData.DiscountData.DiscountPercent)
			if styleMap["discount"].(float64) > 0 {
				styleMap["onDiscount"] = true
				styleMap["price"] = styleMap["discountedPrice"]
			}

		}
		return styleMap, nil
	} else {
		return nil, errors.New("Error in getting PDP response from Maximus ")
	}

}
