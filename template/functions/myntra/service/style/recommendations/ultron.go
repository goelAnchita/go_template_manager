package recommendations

import (
	"actionexecutor/libs/template/functions/myntra/service"
	"actionexecutor/libs/util"
	"encoding/json"
	"errors"
	"github.com/golang/glog"
	"io/ioutil"
	"net/http"
	"strings"
)

type Ultron_Client struct {
	http_client *http.Client
	http_header http.Header
}

const (
	KeepAliveConnection bool   = true
	DSRecommendation    string = "dsRecommendations"
)

var (
	EndURI        string = service.ServiceConf.Styles.Ultron.URL + "/ultron-service/ultron/recommendations"
	transport     *http.Transport
	ultron_client *Ultron_Client
)

func init() {

	transport = &http.Transport{
		DisableKeepAlives:   !KeepAliveConnection,
		MaxIdleConnsPerHost: service.ServiceConf.HTTPMaxIdleConnections,
	}

	header := make(http.Header)
	header.Add("Accept", "application/json")
	header.Add("Connection", "keep-alive")
	header.Add("Content-Type", "application/json")

	client := &http.Client{Transport: transport}
	ultron_client = &Ultron_Client{http_header: header, http_client: client}

}

func NewUltronClient() *Ultron_Client {
	return ultron_client

}

func (client *Ultron_Client) GetRecommendations(styleIds []int64) (recos interface{}, err error) {
	var response *http.Response = nil

	defer func() {
		if response != nil {
			response.Body.Close()
		}
	}()

	requestBodyMap := make(map[string]interface{})
	requestBodyMap["styleIds"] = styleIds
	requestBodyMap["recommendationType"] = make([]string, 1)
	requestBodyMap["recommendationType"].([]string)[0] = DSRecommendation

	jsonBody, err := json.Marshal(requestBodyMap)
	if err != nil {
		return nil, err
	}

	glog.Info("Sending body %+v", string(jsonBody))
	requestBody := string(jsonBody)
	dataBody := strings.NewReader(requestBody)
	request, err := http.NewRequest("POST", EndURI, dataBody)

	if err != nil {
		glog.Errorf("unable to make request: %s", err)
		return nil, err
	}

	request.Header = client.http_header
	response, err = util.HttpRetry(request, client.http_client, requestBody)

	if err != nil {
		if response == nil {
			glog.Info("Something went wrong with error while doing http_client.Do %s", err.Error())
		} else if response != nil {
			glog.Info("Something went wrong with error while doing http_client.Do %s", err.Error(), response.StatusCode, response)
			body, _ := ioutil.ReadAll(response.Body)
			glog.Error(string(body))
		}
		return nil, err
	}

	return responseReply(response)
}

func responseReply(resp *http.Response) (response map[string]interface{}, err error) {

	//glog.Infof("response header %v", resp.Header)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		glog.Errorf("Something went wrong while ioutil.ReadAll with error %s", err.Error())
		return
	}

	var responseData map[string]interface{}

	if err := json.Unmarshal(body, &responseData); err != nil {
		glog.Errorf("Something went wrong while unmarshalling the response body with %s", err.Error())
		return nil, err
	}

	if data, ok := responseData["recommendations"]; ok {
		return data.(map[string]interface{}), nil
	} else {
		return nil, errors.New("No recommendations found")
	}

}
