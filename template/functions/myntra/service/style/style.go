package style

import (
	"actionexecutor/libs/template/functions/myntra/service/style/maximus"
	"actionexecutor/libs/template/functions/myntra/service/style/recommendations"
	"actionexecutor/libs/util/statsd"
	"fmt"
	"github.com/golang/glog"
	"strconv"
)

func GetStyleData(styleid interface{}) (response map[string]interface{}, err error) {

	maximusClient, err := maximus.MaximusClient()
	if err != nil {
		glog.Errorf("Error in getting maximums client %s", err)
		statsd.Increment("workflow.maximusClient.error")
		return nil, err
	}

	var styleId int64
	switch v := styleid.(type) {
	default:
		fmt.Printf("unexpected type %T", v)
	case float64:
		styleId = int64(styleid.(float64))
	case string:
		styleId, err = strconv.ParseInt(styleid.(string), 10, 64)
	case int64:
		styleId = styleid.(int64)
	}

	if err != nil {
		glog.Errorf("Error in getting parsing style ID %s", err)
	}

	defer func() {
		if r := recover(); r != nil {
			glog.Errorf("Error in receiving PDP data from Maximus")
		}
	}()

	styleMap, err := maximusClient.GetStyleData(styleId, maximus.Fields_ALL)
	if err != nil {
		glog.Errorf("Error %v in getting PDP data from Maximus for style ID %v", err, styleId)
	}

	return styleMap, err

}

func GetStyleRecommendations(styleid interface{}, maxStyles int) (response []interface{}) {

	defer func() {
		if r := recover(); r != nil {
			glog.Errorf("Panic attack with error %+v in GetStyleRecommendations", r)

		}
	}()

	var styleId int64
	var err error

	switch v := styleid.(type) {

	case float64:
		styleId = int64(styleid.(float64))
	case string:
		styleId, err = strconv.ParseInt(styleid.(string), 10, 64)
	case int64:
		styleId = styleid.(int64)
	case int:
		styleId = int64(styleid.(int))
	default:
		fmt.Printf("unexpected type %T", v)
	}

	if err != nil {
		glog.Errorf("Error in getting parsing style ID %s in GetStyleRecommendations", err)
		return nil
	}

	styleArray := make([]int64, 1)
	styleArray[0] = styleId

	styleResponse, err := recommendations.NewUltronClient().GetRecommendations(styleArray)
	if err != nil {
		return nil
	} else {
		if recommendationResponse, ok := styleResponse.(map[string]interface{})[strconv.FormatInt(styleId, 10)]; ok {
			if dsReco, ok := recommendationResponse.(map[string]interface{})[recommendations.DSRecommendation]; ok {
				if recommendationDataEntryList, ok := dsReco.(map[string]interface{})["recommendationDataEntryList"]; ok {
					if list, ok := recommendationDataEntryList.([]interface{}); ok {
						if styleIds, ok := list[0].(map[string]interface{})["styleIds"].([]interface{}); ok {
							recoLength := len(styleIds)
							if recoLength > maxStyles {
								recoLength = maxStyles
							} else if recoLength <= 0 {
								return nil
							}
							styleRecommendations := make([]interface{}, recoLength)
							for i := 0; i < recoLength; i++ {
								styleData, err := GetStyleData(styleIds[i])
								if err != nil {
									return nil
								} else {
									styleRecommendations[i] = styleData
								}
							}
							return styleRecommendations

						} else {
							glog.Errorf("Error in conversion of %s", "styleIds")
							return nil
						}
					} else {
						glog.Errorf("Error in conversion of %s", "recommendationDataEntryList")
						return nil
					}

				} else {
					glog.Errorf("Error in conversion of %s", recommendations.DSRecommendation)
					return nil
				}

			} else {
				glog.Errorf("Error in conversion of %s", "recommendationResponse")
				return nil
			}

		} else {
			glog.Errorf("Error in conversion of %s", "styleResponse")
			return nil
		}

	}
	return nil
}
