package dao

import (
	"actionexecutor/libs/template/entity"
	"actionexecutor/libs/util/db/cassandra"
	"actionexecutor/libs/util/statsd"
	"errors"
	"fmt"
	"github.com/gocql/gocql"
	"github.com/golang/glog"
	"strings"
)

const (
	Keyspace      string = "yantra_templates"
	TemplateTable string = "templates"
	VersionTable  string = "template_versions"
)

var (
	initialiseCassandra [3]string = [3]string{
		"CREATE KEYSPACE IF NOT EXISTS yantra_templates " +
			"WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '2'}  " +
			"AND durable_writes = true;",
		"CREATE TABLE IF NOT EXISTS yantra_templates.templates " +
			"(name text, version int, data text, PRIMARY KEY (name, version))" +
			" WITH CLUSTERING ORDER BY (version DESC);",
		"CREATE TABLE IF NOT EXISTS yantra_templates.template_versions " +
			"(name text PRIMARY KEY,version int) ;"}
	session *gocql.Session
	config  cassandra.CassandraConfig
)

type TemplateCassandraDAO struct {
}

func cassandraInit() {
	createsession, err := cassandra.GetSession(config, "")
	if err != nil {
		panic(errors.New("GetSession failed with error " + err.Error()))
	}

	for _, cqlStmt := range initialiseCassandra {
		cqlStmt = strings.Trim(cqlStmt, "\r\n")
		if cqlStmt == "" {
			continue
		}
		glog.Info("Here " + cqlStmt)
		err = createsession.Query(cqlStmt).Exec()
		if err != nil {
			panic(errors.New("CQL execution failed with  " + err.Error()))
		}
	}
	createsession.Close()
}

func NewTemplateCassandraDAO(dbConfig cassandra.CassandraConfig) *TemplateCassandraDAO {
	config = dbConfig
	var err error
	defer func() {
		if r := recover(); r != nil {
			glog.Error(r.(error))
			if session != nil {
				session.Close()
			}
			panic(r)
		}
	}()

	cassandraInit()
	session, err = cassandra.GetSession(dbConfig, Keyspace)
	if err != nil {
		panic(errors.New("GetSession failed with error " + err.Error()))
	}
	return &TemplateCassandraDAO{}
}

func (dao *TemplateCassandraDAO) Create(entity entity.TemplateEntity) (*entity.TemplateEntity, error) {

	queryStatement := `INSERT INTO ` + string(TemplateTable) + ` (name, version, data) VALUES (?, ?, ?)`
	if err := session.Query(queryStatement, entity.Name, entity.Version, entity.Data).Exec(); err != nil {
		return nil, err
	}

	dao.UpdateCurrentTemplateVersion(entity.Name, entity.Version)
	return &entity, nil

}

func (dao *TemplateCassandraDAO) GetTemplateByLatestVersion(templateName string) (*entity.TemplateEntity, error) {

	var name string
	var version int
	var data string

	getTemplateQueryStatementTemplate := "SELECT * FROM %s WHERE name = '%s' limit 1"
	getTemplateQueryStatement := fmt.Sprintf(getTemplateQueryStatementTemplate, string(TemplateTable), templateName)
	if err := session.Query(getTemplateQueryStatement).Consistency(gocql.One).Scan(&name, &version, &data); err != nil {
		glog.Errorf("Error while getting the required template %s : %s", templateName, err)
		statsd.Increment("cassandraDAO.getTemplateByLatestVersion.error")
		return nil, err
	}

	templateEntity := entity.TemplateEntity{Name: name, Version: version, Data: data}
	return &templateEntity, nil
}

func (dao *TemplateCassandraDAO) UpdateCurrentTemplateVersion(templateName string, version int) (*entity.TemplateEntity, error) {

	updateStatementTemplate := "UPDATE %s SET version = %v WHERE name = '%s'"
	updateStatement := fmt.Sprintf(updateStatementTemplate, string(VersionTable), version, templateName)
	if err := session.Query(updateStatement).Exec(); err != nil {
		glog.Error(err)
		return nil, err
	}

	entity, err := dao.GetTemplateByVersion(templateName, version)
	if err != nil {
		return nil, err
	}
	return entity, nil

}

func (dao *TemplateCassandraDAO) GetCurrentTemplateVersion(templateName string) (int, error) {

	var version int

	getTemplateVersionStatementTemplate := "SELECT version FROM %s WHERE name = '%s'"
	getTemplateVersionStatement := fmt.Sprintf(getTemplateVersionStatementTemplate, string(VersionTable), templateName)
	if err := session.Query(getTemplateVersionStatement).Consistency(gocql.One).Scan(&version); err != nil {
		glog.Errorf("Error while getting the required template version %s : %s", templateName, err)
		statsd.Increment("cassandraDAO.getCurrentTemplateVersion.error")
		return -1, err
	}

	return version, nil
}

func (dao *TemplateCassandraDAO) GetTemplateByVersion(templateName string, version int) (*entity.TemplateEntity, error) {
	var name string
	var data string
	var vers int

	getTemplateByVersionStatementTemplate := "SELECT * FROM %s WHERE name = '%s' and version = %v"
	getTemplateByVersionStatement := fmt.Sprintf(getTemplateByVersionStatementTemplate, string(TemplateTable), templateName, version)
	if err := session.Query(getTemplateByVersionStatement).Consistency(gocql.One).Scan(&name, &vers, &data); err != nil {
		glog.Errorf("Error while getting the required template by version %s : %s", templateName, err)
		statsd.Increment("cassandraDAO.GetTemplateByName.error")
		return nil, err
	}

	templateEntity := entity.TemplateEntity{Name: name, Version: vers, Data: data}
	return &templateEntity, nil
}

func (dao *TemplateCassandraDAO) GetCurrentTemplateByName(templateName string) (*entity.TemplateEntity, error) {

	version, err := dao.GetCurrentTemplateVersion(templateName)
	if err != nil || version < 0 {
		templateEntity, err := dao.GetTemplateByLatestVersion(templateName)
		if err != nil {
			return nil, err
		}
		version = templateEntity.Version
	}

	entity, err := dao.GetTemplateByVersion(templateName, version)
	if err != nil {
		return nil, err
	}
	return entity, nil

}

func (dao *TemplateCassandraDAO) FetchTemplatesByName(templateName string) ([]entity.TemplateEntity, error) {
	var name string
	var data string
	var vers int
	var templateEntity entity.TemplateEntity

	getTemplatesByNameStatementTemplate := "SELECT * FROM %s WHERE name = '%s'"
	getTemplateByVersionStatement := fmt.Sprintf(getTemplatesByNameStatementTemplate, string(TemplateTable), templateName)
	rowIter := session.Query(getTemplateByVersionStatement).Consistency(gocql.One).Iter()
	totalVersions := rowIter.NumRows()

	var templateEntities = make([]entity.TemplateEntity, totalVersions, totalVersions)
	rowNum := 0

	if totalVersions == 0 {
		return nil, errors.New("No such template found")
	}
	for rowIter.Scan(&name, &vers, &data) {
		templateEntity = entity.TemplateEntity{Name: name, Version: vers, Data: data}
		templateEntities[rowNum] = templateEntity
		rowNum++
	}
	if err := rowIter.Close(); err != nil {
		glog.Errorf("Error in closing iterator %s", err)
		return templateEntities, err
	}

	return templateEntities, nil
}
