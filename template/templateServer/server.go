package templateServer

import (
	"actionexecutor/libs/template/manager"
	"actionexecutor/libs/util"
	"flag"
	"fmt"
	"github.com/golang/glog"
	"github.com/julienschmidt/httprouter"
	"io/ioutil"
	"net/http"
	"strconv"
)

var (
	Port string
)

type Server struct {
	TemplateManager *manager.TemplateManager
}

func NewServer(manager *manager.TemplateManager) *Server {

	return &Server{manager}
}

func constructResponse(statusCode int, statusMessage interface{}) ([]byte, error) {

	response := make(map[string]interface{})
	response["statusCode"] = statusCode
	response["statusMessage"] = statusMessage
	glog.Infof("Constructing response %+v", response)
	return util.JSONMarshal(response)

}

func sendResponse(w http.ResponseWriter, statusCode int, statusMessage interface{}) {

	w.WriteHeader(statusCode)
	msg, err := constructResponse(statusCode, statusMessage)
	if err != nil {
		glog.Errorf("Error in constructing Response")
		return
	}
	glog.Infof("Sending response %+v", string(msg))
	w.Write([]byte(msg))
}

func (s *Server) registerHTMLTemplate(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	templateName := p.ByName("name")
	templateBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		glog.Error("ERROR WHILE READING FROM REQUEST BODY")
		errMessage := fmt.Sprintf("ERROR OCCURRED WHILE CREATING TEMPLATE: %s", err)
		sendResponse(w, http.StatusBadRequest, errMessage)
		return
	} else {
		templateId, falconideTemplate, variableMap, err := manager.CreateTemplate(string(templateBody), templateName)
		if err != nil {
			glog.Error("ERROR WHILE REGISTERING TEMPLATE WITH FALCONIDE")
			errMessage := fmt.Sprintf("ERROR WHILE REGISTERING TEMPLATE WITH FALCONIDE: %s", err)
			sendResponse(w, http.StatusBadRequest, errMessage)
			return
		} else {
			responseMap := make(map[string]interface{})
			responseMap["template"] = templateId
			responseMap["falconideHTML"] = falconideTemplate
			responseMap["placeholderMap"] = variableMap
			sendResponse(w, http.StatusOK, responseMap)
		}
		return
	}

}

func (s *Server) updateVersionTemplate(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	templateName := p.ByName("name")
	templateCurrentVersion := p.ByName("version")

	version, err := strconv.Atoi(templateCurrentVersion)
	if err != nil {
		glog.Errorf("ERROR WHILE UPDATING TEMPLATE VERSION BECAUSE OF INCORRECT VERSION")
		errMessage := fmt.Sprintf("ERROR WHILE UPDATING TEMPLATE VERSION BECAUSE OF INCORRECT VERSION: %s", err)
		sendResponse(w, http.StatusBadRequest, errMessage)
		return
	}

	templateEntity, err := s.TemplateManager.UpdateTemplateVersion(templateName, version)
	if err != nil {
		glog.Errorf("ERROR WHILE UPDATING TEMPLATE VERSION ")
		errMessage := fmt.Sprintf("ERROR WHILE UPDATING TEMPLATE VERSION: %s", err)
		sendResponse(w, http.StatusBadRequest, errMessage)
		return
	}

	responseMsg := fmt.Sprintf("The template %s has been updated to version %v", templateEntity.Name, templateEntity.Version)
	sendResponse(w, http.StatusOK, responseMsg)
	return
}

func (s *Server) createActionTemplate(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	templateName := p.ByName("name")

	//buf := new(bytes.Buffer)
	//buf.ReadFrom(r.Body)
	//newStr := buf.String()

	templateBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		glog.Error("ERROR WHILE READING FROM REQUEST BODY")
		errMessage := fmt.Sprintf("ERROR OCCURRED WHILE CREATING TEMPLATE: %s", err)
		sendResponse(w, http.StatusBadRequest, errMessage)
		return
	}

	//res := templatedao.TemplateDBObj.Create(newStr)
	res, err := s.TemplateManager.CreateTemplate(templateName, templateBody)
	if err != nil {
		glog.Errorf("ERROR WHILE CREATING TEMPLATE IN MANAGER %s", err)
		errMessage := fmt.Sprintf("ERROR OCCURRED WHILE CREATING TEMPLATE: %s", err)
		sendResponse(w, http.StatusBadRequest, errMessage)
		return
	}

	//glog.Info(res)

	sendResponse(w, http.StatusOK, res)

}

func (s *Server) fetchTemplatesByName(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	templateName := p.ByName("name")
	res, err := s.TemplateManager.FetchTemplatesByName(templateName)

	if err != nil {
		glog.Error("ERROR WHILE FETCHING TEMPLATE FROM MANAGER")
		errMessage := fmt.Sprintf("ERROR OCCURRED WHILE FETCHING TEMPLATE: %s", err)
		sendResponse(w, http.StatusBadRequest, errMessage)
		return
	}

	sendResponse(w, http.StatusOK, res)
}

func deleteActionTemplate(w http.ResponseWriter, _ *http.Request, ps httprouter.Params) {
	//glog.Info("deleteActionTemplate\n")
	id := ps.ByName("id")
	fmt.Println("id : " + id)
	//templatedao.TemplateDBObj.Delete(id)

}

func (s *Server) StartTemplateServer(port string) error {

	glog.Infof("Started template server")
	healthCheck := "/actionexecutor/healthcheck"

	router := httprouter.New()
	router.POST("/createTemplate/:name", s.createActionTemplate)
	router.POST("/registerHtml/:name", s.registerHTMLTemplate)
	router.GET("/fetchTemplate/:name", s.fetchTemplatesByName)
	router.PUT("/deleteTemplate/:id", deleteActionTemplate)
	router.PUT("/updateTemplateVersion/:name/:version", s.updateVersionTemplate)

	inLB := false
	router.GET(healthCheck, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		glog.V(3).Infof("InLB = %s", strconv.FormatBool(inLB))
		if inLB {
			fmt.Fprintln(w, "OK")
		} else {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		}
	})

	router.GET(healthCheck+"/bringInLB", func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		glog.V(0).Info(string(r.RequestURI))
		inLB = true
		glog.V(0).Infof("InLB = %s", strconv.FormatBool(inLB))
		fmt.Fprintln(w, "OK")

	})
	router.GET(healthCheck+"/bringOutOfLB", func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		glog.V(0).Info(string(r.RequestURI))
		inLB = false
		glog.V(0).Infof("InLB = %s", strconv.FormatBool(inLB))
		fmt.Fprintln(w, "OK")

	})

	return http.ListenAndServe(":"+port, router)

}

func init() {
	flag.Parse()
}
