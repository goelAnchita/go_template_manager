package manager

import (
	"actionexecutor/libs/template/dao"
	"actionexecutor/libs/template/entity"
	"github.com/golang/glog"
	"text/template"
)

type TemplateManager struct {
	TemplateDAO   *dao.TemplateCassandraDAO
	TemplateCache *Cache
}

func NewTemplateManager(dao *dao.TemplateCassandraDAO, cache *Cache) *TemplateManager {
	return &TemplateManager{TemplateDAO: dao, TemplateCache: cache}
}

func (manager *TemplateManager) CreateTemplate(templateName string, templateBody []byte) (*entity.TemplateEntity, error) {

	templateEntity := entity.TemplateEntity{}
	existingTemplateEntity, err := manager.GetLatestTemplateByName(templateName)

	if err != nil {
		templateEntity.Version = templateEntity.Version + 1
	} else {
		templateEntity.Version = existingTemplateEntity.Version + 1
	}

	templateEntity.Name = templateName
	templateEntity.Data = string(templateBody)

	createEvent := Event{Action: CREATE, Template: templateEntity}

	err = manager.TemplateCache.reflectChanges(createEvent)
	if err != nil {
		glog.Error(err)
		return nil, err

	}

	finalEntity, err := manager.TemplateDAO.Create(templateEntity)

	if err != nil {
		glog.Error(err)
		return nil, err

	}

	createEvent.Template = *finalEntity
	manager.TemplateCache.Notifier.Notify(createEvent)
	return finalEntity, nil

}

func (manager *TemplateManager) FetchTemplatesByName(templateName string) ([]entity.TemplateEntity, error) {
	templateEntities, err := manager.TemplateDAO.FetchTemplatesByName(templateName)
	if err != nil {
		glog.Error("ERROR OCCURRED WHILE FETCHING TEMPLATE FROM DAO")
		return nil, err
	}
	return templateEntities, nil
}

func (manager *TemplateManager) GetLatestTemplateByName(templateName string) (*entity.TemplateEntity, error) {

	templateEntity, err := manager.TemplateDAO.GetTemplateByLatestVersion(templateName)
	if err != nil {
		glog.Error("ERROR OCCURRED WHILE FETCHING TEMPLATE FROM DAO")
		return nil, err
	}
	return templateEntity, nil
}

func (manager *TemplateManager) GetCurrentTemplateByName(templateName string) (*entity.TemplateEntity, error) {

	templateEntity, err := manager.TemplateDAO.GetCurrentTemplateByName(templateName)
	if err != nil {
		glog.Error("ERROR OCCURRED WHILE FETCHING TEMPLATE FROM DAO")
		return nil, err
	}
	return templateEntity, nil
}
func (manager *TemplateManager) UpdateTemplateVersion(templateName string, version int) (*entity.TemplateEntity, error) {

	entity, err := manager.TemplateDAO.UpdateCurrentTemplateVersion(templateName, version)
	if err != nil {
		return nil, err
	}

	manager.TemplateCache.Notifier.Notify(Event{Action: UPDATE, Template: *entity})
	return entity, nil
}

func (manager *TemplateManager) GetParsedTemplate(templateName string) (*template.Template, error) {

	template, err := manager.TemplateCache.GetTemplate(templateName)

	if err != nil {
		template, err = manager.TemplateCache.ParseTemplate(manager, templateName)
		if err != nil {
			return nil, err
		}
	}

	return template, nil
}
