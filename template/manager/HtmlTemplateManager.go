package manager

import (
	"actionexecutor/libs/util"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/golang/glog"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

type FalconideHtmlTemplate struct {
	ApiKey       string `json:"api_key,omitempty"`
	TemplateName string `json:"templatename,omitempty"`
	Content      string `json:"content,omitempty"`
	Type         string `json:"type,omitempty"`
	TemplateId   string `json:"templateid,omitempty"`
}

type FalconideEmailConf struct {
	PlaceHolderTag      string
	TemplateRegisterURL string
	APIKey              string
}

type Placeholder struct {
	count          int
	placeholderMap map[string]string
}

var (
	FalconideConf          FalconideEmailConf
	placeHolderPrefix      string = FalconideConf.PlaceHolderTag
	placehodlerSuffix      string = FalconideConf.PlaceHolderTag
	placeHolderRegExp      string = ""
	placeHolderFormat      string = `[%% %v %%]`
	APIKey                 string = FalconideConf.APIKey
	transport              *http.Transport
	falconide_client       *FalconideClient
	KeepAliveConnection    bool   = true
	templateRegisterURL    string = "/falconapi/manage.falconide"
	registerTemplateType   string = "templateadd"
	registerTemplateMethod string = "POST"
	registerTemplateURL    string = FalconideConf.TemplateRegisterURL + templateRegisterURL
)

type FalconideClient struct {
	http_client *http.Client
	http_header http.Header
}

func init() {

	placeHolderPrefix = "<" + placeHolderPrefix + ">"
	placehodlerSuffix = "</" + placehodlerSuffix + ">"
	placeHolderRegExp = fmt.Sprintf("%s(.*?)%s", placeHolderPrefix, placehodlerSuffix)

	transport = &http.Transport{DisableKeepAlives: !KeepAliveConnection}

	header := make(http.Header)
	header.Add("Content-Type", "application/x-www-form-urlencoded")
	header.Add("Accept", "application/json")
	header.Add("Connection", "keep-alive")

	client := &http.Client{Transport: transport}
	falconide_client = &FalconideClient{http_header: header, http_client: client}

}

func GetFalconideClient() *FalconideClient {
	return falconide_client
}

func (client *FalconideClient) RegisterTemplate(falconideTemplate string, templateName string) (string, error) {

	htmlTemplateObject := FalconideHtmlTemplate{ApiKey: APIKey, TemplateName: templateName,
		Content: falconideTemplate, Type: registerTemplateType}

	jsonByte, err := util.JSONMarshal(htmlTemplateObject)

	if err != nil {
		glog.Errorf("Error in marshalling HtmlTemplate Object %v", htmlTemplateObject)
		return "", err
	} else {
		values := url.Values{"data": {string(jsonByte)}}
		templateResponse, err := client.HTTPRequest((values.Encode()), registerTemplateURL, registerTemplateMethod)
		if err != nil {
			return "", err
		} else {
			if templateResponse["status"] == "success" {
				templateId := templateResponse["templateid"].(string)
				return templateId, nil
			} else if templateResponse["status"] == "error" {
				return "", errors.New(templateResponse["message"].(string))
			} else {
				return "", errors.New("Unknown response from falconide")
			}
		}
	}

}

func CreateTemplate(html string, templateName string) (templateId string, falconideTemplate string, placeholderMap map[string]string, err error) {

	re := regexp.MustCompile(`[\r\n\t]+`)
	html = re.ReplaceAllString(html, "")
	//glog.Infof("the html is ----------------- %s ", html)
	placeholderMap, falconideTemplate, err = extractPlaceholders(html)
	if err != nil {
		return
	}
	templateId, err = GetFalconideClient().RegisterTemplate(falconideTemplate, templateName)
	return
}

func (placeholder *Placeholder) createPlaceholder(expression string) (placeholderKey string) {

	phKey := fmt.Sprintf("placeholder_%d", placeholder.count)
	ph := fmt.Sprintf(placeHolderFormat, phKey)
	codeSegment := strings.TrimSpace(expression)
	codeSegment = strings.TrimPrefix(codeSegment, placeHolderPrefix)
	codeSegment = strings.TrimSuffix(codeSegment, placehodlerSuffix)
	placeholder.placeholderMap[phKey] = codeSegment
	placeholder.count++
	return ph
}

func extractPlaceholders(html string) (map[string]string, string, error) {

	placeholder := Placeholder{}
	placeholder.placeholderMap = make(map[string]string)
	htmlResult := regexp.MustCompile(placeHolderRegExp).ReplaceAllStringFunc(html, placeholder.createPlaceholder)
	//fmt.Print(htmlResult)
	//fmt.Printf("%v", placeholder.placeholderMap)
	return placeholder.placeholderMap, htmlResult, nil
}

func (client *FalconideClient) HTTPRequest(data string, EndURI string, method string) (map[string]interface{}, error) {
	var response *http.Response = nil

	defer func() {
		if response != nil {
			response.Body.Close()
		}
	}()

	dataBody := strings.NewReader(data)

	request, err := http.NewRequest(method, EndURI, dataBody)

	if err != nil {
		glog.Errorf("Error while creating new HTTP request %v", err)
		return nil, err
	} else {
		request.Header = client.http_header
		response, err = util.HttpRetry(request, client.http_client, data)

		if err != nil {
			if response == nil {
				glog.Errorf("Something went wrong with error while doing http_client.Do %s", err.Error())
			} else if response != nil {
				glog.Errorf("Something went wrong with error while doing http_client.Do %s", err.Error(), response.StatusCode, response)
				body, _ := ioutil.ReadAll(response.Body)
				glog.Info(string(body))
			}
			return nil, err
		}

		return responseReply(response)
	}

}

func responseReply(resp *http.Response) (result map[string]interface{}, err error) {

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		glog.Errorf("Something went wrong while ioutil.ReadAll with error %s", err.Error())
		return nil, err
	} else {
		result = make(map[string]interface{})
		err = json.Unmarshal(body, &result)

		if err == nil && resp.StatusCode == 200 {
			return result, nil
		} else {
			errorString := fmt.Sprintf("Falconide error response code %v", resp.StatusCode)
			return nil, errors.New(errorString)
		}
	}

}
