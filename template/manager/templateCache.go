package manager

import (
	"actionexecutor/libs/template/entity"
	"actionexecutor/libs/util/kafka"
	"actionexecutor/libs/util/statsd"
	"encoding/json"
	"errors"
	"github.com/Shopify/sarama"
	"github.com/golang/glog"
	"os"
	"sync"
	"text/template"
)

type Action int

type Event struct {
	Action   Action
	Template entity.TemplateEntity
}

const (
	CREATE Action = iota
	REMOVE
	UPDATE
)

var config CacheConfig

type CacheConfig struct {
	CacheUpdateConsumer kafka.KafkaConsumerConfig
	CacheUpdateProducer kafka.KafkaProducerConfig
}

type Cache struct {
	GoTemplate *template.Template
	mutex      sync.RWMutex
	Notifier   *CacheUpdatesNotifier
}

func NewTemplateCache(template *template.Template, cacheConfig CacheConfig) *Cache {
	config = cacheConfig
	updateObserver := InitUpdateObserver()
	updateNotifier := InitUpdateNotifier()
	templateCache := &Cache{GoTemplate: template, Notifier: updateNotifier}
	go updateObserver.StartConsumer(templateCache.UpdateCache)
	return templateCache
}

func InitUpdateObserver() *CacheUpdateObserver {
	cacheKafkaConfig := config.CacheUpdateConsumer
	consumerGroupName, err := os.Hostname()
	if err != nil {
		glog.Errorf("Error in getting the hostname")
		return nil
	}
	cacheKafkaConfig.ConsumerGroup = consumerGroupName
	updatesObserver := InitObserver(cacheKafkaConfig)
	return updatesObserver
}

func InitUpdateNotifier() *CacheUpdatesNotifier {
	kafkaConfig := config.CacheUpdateProducer
	updatesNotifier := InitNotifier(kafkaConfig)
	return updatesNotifier
}

func (cache *Cache) ParseTemplate(templateManager *TemplateManager, templateName string) (t *template.Template, err error) {
	cache.mutex.Lock()
	t = cache.GoTemplate.Lookup(templateName)
	if t == nil {
		glog.Infof("Cache didnt have the template %s", templateName)
		entity, err := templateManager.GetCurrentTemplateByName(templateName)
		if err == nil {
			t, err = cache.GoTemplate.New(templateName).Parse(entity.Data)
		}
	}
	cache.mutex.Unlock()

	return
}

func (cache *Cache) GetTemplate(templateName string) (*template.Template, error) {
	cache.mutex.RLock()
	template := cache.GoTemplate.Lookup(templateName)
	cache.mutex.RUnlock()

	if template == nil {
		glog.Error("######## template not found in cache")
		return nil, errors.New("template not found")
	}

	return template, nil
}

func (cache *Cache) UpdateCache(msg *sarama.ConsumerMessage) {
	event := Event{}
	err := json.Unmarshal(msg.Value, &event)
	if err != nil {
		glog.Errorf("Error in unmarshalling the update event")
	}
	if err != nil {
		statsd.Increment("templateUpdateObserver.UpdateEventError")
	} else {
		cache.reflectChanges(event)
		statsd.Increment("templateUpdateObserver.UpdateReceived")
	}
}

func (cache *Cache) reflectChanges(e Event) (err error) {
	templateEntity := e.Template
	if e.Action != REMOVE {
		cache.mutex.Lock()
		glog.Infof("Reflecting changes in the template for %s ", templateEntity.Name)
		_, err = cache.GoTemplate.New(templateEntity.Name).Parse(templateEntity.Data)
		cache.mutex.Unlock()
		if err != nil {
			glog.Error("While parsing template with id "+templateEntity.Name, err.Error())
		}
	}
	return
}
