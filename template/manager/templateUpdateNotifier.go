package manager

import (
	"actionexecutor/libs/util/kafka"
	"encoding/json"
	"github.com/Shopify/sarama"
	"github.com/golang/glog"
)

type CacheUpdatesNotifier struct {
	kafkaProducer *kafka.KafkaProducer
	topic         string
}

func InitNotifier(config kafka.KafkaProducerConfig) (notifier *CacheUpdatesNotifier) {

	kafkaProducer := kafka.InitProducer(config)
	notifier = &CacheUpdatesNotifier{kafkaProducer: kafkaProducer, topic: config.Topic}
	return
}

func (notifier *CacheUpdatesNotifier) Notify(event Event) {

	eventData, err := json.Marshal(event)
	if err != nil {
		glog.Errorf("Error in marshalling the event")
		return
	}

	glog.Infof("Notifiying template change with %v", event)
	eventMessage := &sarama.ProducerMessage{Topic: notifier.topic, Value: sarama.ByteEncoder(eventData)}
	notifier.kafkaProducer.PushMessageToKafka(eventMessage)
}
