package manager

import (
	"actionexecutor/libs/util/kafka"
	"flag"
	"github.com/Shopify/sarama"
)

type CacheUpdateObserver struct {
	kafkaConsumer *kafka.KafkaConsumer
}

func InitObserver(consumerConfig kafka.KafkaConsumerConfig) *CacheUpdateObserver {

	kafkaConsumer := kafka.InitConsumer(consumerConfig)
	updateObserver := CacheUpdateObserver{kafkaConsumer: kafkaConsumer}
	return &updateObserver
}

func (actionConsumer *CacheUpdateObserver) StartConsumer(consumerFunction func(message *sarama.ConsumerMessage)) {

	actionConsumer.kafkaConsumer.StartConsumer(consumerFunction)

}

func (updateConsumer *CacheUpdateObserver) StopConsumer() {
	updateConsumer.kafkaConsumer.StopConsumer()
}

func init() {
	flag.Parse()
}
