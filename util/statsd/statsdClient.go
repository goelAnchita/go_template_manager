package statsd

import (
	"github.com/golang/glog"
	"gopkg.in/alexcesaro/statsd.v2"
	"os"
	"time"
)

type StatsdConf struct {
	Address  string
	Prefix   string
	Attempts int
	Backoff  time.Duration
}

var (
	statsdClient *statsd.Client
	config       StatsdConf
	hostname, _  = os.Hostname()
)

func connectToStatsd(retry_attempts int) {
	statsd_prefix := hostname + "." + config.Prefix
	var err error

RECONNECT:

	//glog.Infof("statsd %v", retry_attempts)
	if retry_attempts == 0 {
		glog.Info("Cannot connect to statsd at %s", config.Address)
		return
	} else {
		statsdClient, err = statsd.New(
			statsd.Address(config.Address),
			statsd.Prefix(statsd_prefix),
		)

		if err != nil {
			glog.Errorf("Unable to create a Statsd Client with error %s", err)
			time.Sleep(config.Backoff * time.Second)
			retry_attempts--
			goto RECONNECT
		}
	}
}

func InitStatsd(config StatsdConf) {
	retry_attempts := config.Attempts
	connectToStatsd(retry_attempts)
}

func Increment(bucket string) {

	if statsdClient == nil {
		connectToStatsd(1)
	}
	if statsdClient != nil {
		statsdClient.Increment(bucket)
	}

}
