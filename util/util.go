package util

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/golang/glog"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/http/httptrace"
	"strings"
	"time"
)

const (
	totalAttempts int = 3
)

type stop struct {
	error
}

var (
	trace *httptrace.ClientTrace
)

func init() {
	trace = &httptrace.ClientTrace{
		GotConn: func(connInfo httptrace.GotConnInfo) {
			glog.Infof("Conn: %+v, LocalAddr: %+v, RemoteAddr: %+v\n", connInfo, connInfo.Conn.LocalAddr(), connInfo.Conn.RemoteAddr())
		},
	}
}

func Retry(client *http.Client, attempts int, sleep time.Duration, f func(*http.Client) (*http.Response, error)) (*http.Response, error) {

	resp, err := f(client)
	if err != nil {

		if s, ok := err.(stop); ok {
			// Return the original error for later checking
			return nil, s.error
		}

		if attempts--; attempts > 0 {
			glog.Infof("Retrying another time!!")
			// Add some randomness to prevent creating a Thundering Herd
			jitter := time.Duration(rand.Int63n(int64(sleep)))
			sleep = sleep + jitter/2

			time.Sleep(sleep)
			return Retry(client, attempts, 2*sleep, f)
		}
		return nil, err
	} else {
		return resp, nil
	}

}

func GetValueByKey(dictionary map[string]interface{}, keys []string) (value interface{}) {

	//glog.Info(len(keys))
	for index, key := range keys {
		if value, ok := dictionary[key]; ok {
			if index < (len(keys) - 1) {
				if dictionary, ok = value.(map[string]interface{}); ok {
				} else {
					return nil
				}
			} else {
				return value
			}

		} else {
			return nil
		}
	}
	return nil
}

func JSONMarshal(t interface{}) ([]byte, error) {
	buffer := &bytes.Buffer{}
	encoder := json.NewEncoder(buffer)
	encoder.SetEscapeHTML(false)
	err := encoder.Encode(t)
	return buffer.Bytes(), err
}

func HttpRetry(request *http.Request, client *http.Client, requestBody string) (*http.Response, error) {
	//glog.Infof("Requesting %s", request.URL)
	request = request.WithContext(httptrace.WithClientTrace(request.Context(), trace))
	request.Close = false
	sleep := time.Second
	attempts := totalAttempts
	var err error = errors.New("No http attempts made")
	for attempts > 0 {

		resp, err := client.Do(request)
		attempts--
		if err != nil {
			jitter := time.Duration(rand.Int63n(int64(sleep)))
			sleep = sleep + jitter/2
			time.Sleep(sleep)
			sleep = 2 * sleep
			if resp != nil {
				io.Copy(ioutil.Discard, resp.Body)
				resp.Body.Close()
			}
		} else {
			s := resp.StatusCode
			switch {
			case s >= 500:
				body, _ := ioutil.ReadAll(resp.Body)
				err = fmt.Errorf("Service error: %v [%v, %s]", resp.Status, resp.StatusCode, string(body))
				if resp != nil {
					resp.Body.Close()
				}
				if requestBody != "" {
					dataBody := (strings.NewReader(requestBody))
					request.Body = ioutil.NopCloser(dataBody)

				}
				jitter := time.Duration(rand.Int63n(int64(sleep)))
				sleep = sleep + jitter/2
				time.Sleep(sleep)
				sleep = 2 * sleep
			case s >= 400:
				body, _ := ioutil.ReadAll(resp.Body)
				err = stop{fmt.Errorf("Service error: %v [%v %s]", resp.Status, resp.StatusCode, string(body))}
				if resp != nil {
					resp.Body.Close()
				}
				return nil, err
			default:
				//glog.Infof("response header %+v", resp.Header)
				return resp, nil
			}

		}

	}
	glog.Errorf("HTTP Retry : Got this error %s", err)
	return nil, err

}
