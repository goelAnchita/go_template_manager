package helper

import "time"

type Time struct {
}

func (self Time) Today(format string) string {
	t := time.Now()
	t.Format(time.RFC3339)
	return t.Format(format)
}
