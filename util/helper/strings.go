package helper

import "strings"

type String struct {
}

func (t String) Join(sep string, args ...string) string {
	return strings.Join(args, sep)
}
