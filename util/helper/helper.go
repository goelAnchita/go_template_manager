package helper

var (
	HelperSingleton *Helper
)

type Helper struct {
	StringHelper String
	TimeHelper   Time
}

func init() {
	HelperSingleton = new(Helper)
}
