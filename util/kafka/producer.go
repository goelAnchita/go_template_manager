package kafka

import (
	"github.com/Shopify/sarama"
	"github.com/golang/glog"
	"log"
	"os"
	"os/signal"
	"strings"
)

type KafkaProducerConfig struct {
	Topic string
	Hosts string
}

type KafkaProducer struct {
	producer sarama.AsyncProducer
	signals  chan os.Signal
	done     chan bool
}

func InitProducer(config KafkaProducerConfig) *KafkaProducer {

	kafkaProducer := &KafkaProducer{}

	producerConfig := sarama.NewConfig()
	brokerList := strings.Split(config.Hosts, ",")
	producer, err := sarama.NewAsyncProducer(brokerList, producerConfig)

	if err != nil {
		log.Fatalln("Failed to start Sarama producer:", err)
	}

	kafkaProducer.producer = producer
	kafkaProducer.signals = make(chan os.Signal, 1)
	kafkaProducer.done = make(chan bool, 1)
	signal.Notify(kafkaProducer.signals, os.Interrupt)

	go func() {
		for err := range producer.Errors() {
			glog.Errorf("Failed to notify update event: %v", err)
		}
	}()

	return kafkaProducer
}

func (producer *KafkaProducer) PushMessageToKafka(msg *sarama.ProducerMessage) {

	producer.producer.Input() <- msg

}
