package kafka

import (
	"actionexecutor/libs/util/statsd"
	"github.com/Shopify/sarama"
	"github.com/bsm/sarama-cluster"
	"github.com/golang/glog"
	"os"
	"os/signal"
	"strings"
	"time"
)

type KafkaConsumer struct {
	consumer *cluster.Consumer
	signals  chan os.Signal
	done     chan bool
}

type KafkaConsumerConfig struct {
	Hosts         string
	Topics        string
	Backoff       time.Duration
	ConsumerGroup string
}

type MessageConsumeFunction func(msg *sarama.ConsumerMessage)

// Auto balancing consumer connection
func InitConsumer(kafkaConfig KafkaConsumerConfig) *KafkaConsumer {

	kafkaConsumer := &KafkaConsumer{}

RECONNECT:

	clusterConfig := cluster.NewConfig()
	clusterConfig.Group.Return.Notifications = true

	consumer, err := cluster.NewConsumer(strings.Split(kafkaConfig.Hosts, ","), kafkaConfig.ConsumerGroup,
		strings.Split(kafkaConfig.Topics, ","), clusterConfig)

	if err != nil {
		glog.Errorf("Kafka consumer error %#v :: %s\n", consumer, err)
		time.Sleep(time.Duration(kafkaConfig.Backoff) * time.Second)
		goto RECONNECT
	}

	glog.Infof("Smart Kafka consumer connnected for topics %s\n", kafkaConfig.Topics)

	kafkaConsumer.signals = make(chan os.Signal, 1)
	kafkaConsumer.done = make(chan bool, 1)
	signal.Notify(kafkaConsumer.signals, os.Interrupt)
	kafkaConsumer.consumer = consumer
	return kafkaConsumer
}

func (kafkaConsumer *KafkaConsumer) StartConsumer(consumerFunction MessageConsumeFunction) {
	glog.Info("KafkaConsumer::Start")
	consumer := kafkaConsumer.consumer
	for {
		select {
		case msg, more := <-consumer.Messages():
			if more {
				//glog.Infof("received from kafka %s/%d/%d\t%s\n", msg.Topic, msg.Partition, msg.Offset, msg.Value)
				consumerFunction(msg)
				consumer.MarkOffset(msg, "") // mark message as processed

			}
		case err, more := <-consumer.Errors():
			if more {
				glog.Infof("Error: %s\n", err.Error())
				statsd.Increment("kafkaConsumer.Error")
			}
		case ntf, more := <-consumer.Notifications():
			if more {
				glog.Infof("Rebalanced: %+v\n", ntf)
			}
		case <-kafkaConsumer.signals:
			return
		case <-kafkaConsumer.done:
			consumer.Close()
		}
	}
}

func (kafkaConsumer *KafkaConsumer) StopConsumer() {
	kafkaConsumer.done <- true
}
