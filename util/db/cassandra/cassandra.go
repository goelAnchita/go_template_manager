package cassandra

import (
	"github.com/gocql/gocql"
	"github.com/golang/glog"
	"strings"
)

type CassandraConfig struct {
	Keyspace    string
	Hosts       string
	Consistency string
}

func getCassandraHosts(cassandraHosts string) (hosts []string) {

	host_string := cassandraHosts
	hosts = strings.Split(host_string, ",")
	return
}

func GetSession(cassandraConfig CassandraConfig, keySpace string) (*gocql.Session, error) {

	hosts := getCassandraHosts(cassandraConfig.Hosts)
	cluster := gocql.NewCluster(hosts...)
	if keySpace != "" {
		cluster.Keyspace = keySpace
	}

	cluster.Consistency = gocql.ParseConsistency(cassandraConfig.Consistency)
	session, err := cluster.CreateSession()

	if err != nil {
		glog.Error("ERROR CONNECTING TO CASSANDRA", err)
		return nil, err

	}
	return session, nil
}
