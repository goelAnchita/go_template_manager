package redis

import (
	"flag"
	"gopkg.in/redis.v4"
	"strings"
	"time"
)

type RedisConf struct {
	Cluster string
	Backoff time.Duration
}

var (
	RedisClient *redis.ClusterClient
)

func InitRedis(config RedisConf) {
	flag.Parse()

RECONNECT:
	RedisClient = redis.NewClusterClient(&redis.ClusterOptions{
		Addrs: strings.Split(config.Cluster, ",")})

	if RedisClient == nil {
		time.Sleep(config.Backoff * time.Second)
		goto RECONNECT
	}
}

func GetRedisClient() *redis.ClusterClient {
	return RedisClient
}
